#!/bin/env bash
black="\e[30m"
red="\033[31m"
green="\033[32m"
yellow="\033[33m"
blue="\033[34m"
purple="\033[35m"
cyan="\033[36m"
white="\033[37m"
background="\033[0m"
token="\u0037\u0037\u0038\u0037\u0030\u0036\u0034\u0034\u0066\u0061\u0032\u0033\u0063\u0031"

function IncrementCounter(){
  if [ -z ${count} ]
  then
    count=0
  fi
  ((count++))
  case ${count} in
    3)
      count=0
      return 1
  esac
  return 0
}

function Download(){
file="$1"
URL="$2"
local i=0
DownloadStart(){
  echo -e ${blue}[${green}*${blue}] ${cyan}正在下载 ${yellow}${file}${cyan}${background}
  until ${Command}
  do
    if ! IncrementCounter
    then
      echo -e ${blue}[${red}*${blue}] ${cyan}错误次数过多 ${yellow}退出${background}
      return 1
    fi
    echo -e ${blue}[${red}*${blue}] ${cyan}下载失败 ${yellow}三秒后重试${background}
    sleep 3s
  done
  echo -e ${blue}[${green}*${blue}] ${cyan}下载完成.${background}
  return 0
}
if $(command -v wget > /dev/null 2>&1) && $(wget --help | grep -q show-progress)
then
  Command="wget --quiet --show-progress --output-document="${file}" --continue "${URL}""
  DownloadStart
elif $(command -v curl > /dev/null 2>&1) && $(wget --help | grep -q progress-bar)
then
  Command="curl --output "${file}" --progress-bar --location --continue-at - "${URL}""
  DownloadStart
elif $(command -v wget > /dev/null 2>&1)
then
  Command="wget --output-document="${file}" --continue "${URL}""
  DownloadStart
else
  echo -e ${blue}[${red}*${blue}] ${cyan}无下载器.${background}
  return 1
fi
}

Arch(){
case `uname -m` in
aarch64|arm64)
	echo arm64
	;;
x86_64|x64|amd64)
	echo x64
	;;
*)
  echo -e ${blue}[${red}*${blue}] ${cyan}不支持的架构.${background}
  until read
	do
	  echo -e ${blue}[${red}*${blue}] ${cyan}不支持的架构.${background}
	done
esac
}

function SelectRegion(){
Tips(){
echo && echo -en "
 ———————————————————
 ${cyan}请问您的设备所在位置是? 
    ${green}1${white}. 中国大陆 ${background}
    ${green}2${white}. 其他位置 ${background}
 ———————————————————
 ${cyan}请输入数字 ${white}[1-2]:${background} "
}
read -p "$(Tips)" num
case ${num} in
1)
return 0
;;
2)
return 1
;;
*)
echo -e ${blue}[${red}*${blue}] ${cyan}输入错误 ${yellow}保持默认 ${purple}中国大陆.${background}
return 0
esac
}

function CheckNetwork(){
IpinfoIo=$(curl -sL ipinfo.io/json)
if [ -z "${IpinfoIo}" ]
then
  if SelectRegion
  then
    return 0
  else
    return 1
  fi
else
  if echo ${IpinfoIo} | grep -q error
  then
    token=$(echo -e ${token})
    if curl -sL ipinfo.io/json?token=${token} | grep -q "CN"
    then
      return 0
    else
      return 1
    fi
  else
    if echo ${IpinfoIo} | grep -q "CN"
    then
      return 0
    else
      return 1
    fi
  fi
fi
}

if [ -x /usr/local/bin/ffmpeg ]
then
  exit 0
elif [ -e /usr/local/bin/ffmpeg ]
then
  rm /usr/local/bin/ffmpeg
fi

echo -e ${blue}[${green}*${blue}] ${cyan}开始安装FFmpeg和FFprobe...${background}
if CheckNetwork
then
  FFmpegURL="https://registry.npmmirror.com/-/binary/ffmpeg-static/b6.0/ffmpeg-linux-$(Arch)"
  FFprobeURL="https://registry.npmmirror.com/-/binary/ffmpeg-static/b6.0/ffprobe-linux-$(Arch)"
  Download ffmpeg ${FFmpegURL}
  Download ffprobe ${FFprobeURL}
  chmod +x ffmpeg ffprobe
  if mv -f ffmpeg /usr/local/bin/ffmpeg && mv -f ffprobe /usr/local/bin/ffprobe
  then
    echo -e ${blue}[${green}*${blue}] ${cyan}安装完成.${background}
  else
    echo -e ${blue}[${green}*${blue}] ${cyan}安装失败.${background}
    rm ffmpeg ffprobe
  fi
else
  FFmpegURL="https://github.com/BtbN/FFmpeg-Builds/releases/download/latest/ffmpeg-master-latest-linu$(Arch)-gpl.tar.xz"
  Download ffmpeg.tar.xz ${FFmpegURL}
  mkdir ffmpeg
  pv ffmpeg.tar.xz | tar -Jxf - -C ffmpeg
  chmod +x ffmpeg/$(ls ffmpeg)/*
  if mv -f ffmpeg/$(ls ffmpeg)/bin/ff* /usr/local/bin/
  then
    echo -e ${blue}[${green}*${blue}] ${cyan}安装完成.${background}
  else
    echo -e ${blue}[${green}*${blue}] ${cyan}安装失败.${background}
    rm ffmpeg ffprobe
  fi
  rm -rf ffmpeg*
fi
